# CKAN Extension for the European Data Portal

* Running here: https://www.europeandataportal.eu/data

## Development Installation

This guide is only suitable for local development installation. Do not use in production. 

### Prerequisites

* Tested with Ubuntu 16.04.4 LTS
* Install PostgreSQL 9.5 (`sudo apt-get install postgresql-9.5`)


### Install Solr with JTS plugin

* Download Solr 6.5.1
* E.g. from here: http://archive.apache.org/dist/lucene/solr/6.5.1/solr-6.5.1.zip
* No installation needed, just unpack to a writeable folder
* Install JTS Plugin
    * Download https://repo1.maven.org/maven2/com/vividsolutions/jts-core/1.14.0/jts-core-1.14.0.jar
    * Copy the JAR to `solr-6.5.1/server/solr-webapp/webapp/WEB-INF/lib`
* Solr can be started with `./bin/solr start`
* Follow instructions here: https://github.com/ckan/ckan/wiki/Install-and-use-Solr-6.5-with-CKAN 
  * Section "Create and configure the core"


### Install CKAN

* Install CKAN 2.5.7 according to the official documentation
* http://docs.ckan.org/en/2.5/maintaining/installing/install-from-source.html
* Important remarks:
  * Install CKAN in your home directory `/home/user/edp`
  * For this tutorial we assume the directory 
  * Use `pip install -e 'git+https://github.com/ckan/ckan.git@ckan-2.5.7#egg=ckan`
  * After Step 2 execute `pip install setuptools==18.4`
  * Step 4: Create the config file `development.ini` in the folder `/home/user/edp/src/ckan`
  * Skip step 5, Solr is already installed
  * Don't forget to link the Solr schema.xml correctly
  * Skip step 7 and 8
 * Test if CKAN can be started
 * Stop CKAN
 
### Install CKAN Spatial Extension

* Install the CKAN Spatial Extension in `/home/user/edp`
  * http://docs.ckan.org/projects/ckanext-spatial/en/latest/
* Tested with commit `2acf66b110ba534750cab754a50566505ba88d83`
* Use the following libraries:
  * `postgresql-9.5-postgis-2.2 python-dev libxml2-dev libxslt1-dev libgeos-c1v5`
 

### Install CKAN EDP Extension

* Follow the following commands to install the Extension
```
$: cd /home/user/edp/bin
$: source activate
(edp)$: cd ..
(edp)$: pip install -e git+https://gitlab.com/european-data-portal/ckanext-edp.git#egg=ckanext-eodp
(edp)$: cd src/ckanext-eodp
(edp)$: pip install -r requirements.txt
(edp)$: python setup.py develop
(edp)$: cp config.sample.ini config.ini
```

* Edit the `config.ini` for custom settings (the default settings will work as well)
* Override all fields in the `development.ini` listed in `ckan.sample.ini`

### Start CKAN

```
(edp)$: cd /home/user/edp/src/ckan
(edp)$: paster serve /home/user/edp/src/ckan/development.ini
```

* Browse to `http://localhost:5000`
from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
    name='ckanext-eodp',
    version=version,
    description="Euro Open Data Portal",
    long_description='''
    ''',
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='',
    author_email='',
    url='',
    license='',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.eodp'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points='''
        [ckan.plugins]
        # Add plugins here, e.g.
        eodp_theme=ckanext.eodp.theme_plugin:ThemePlugin
        eodp_dataset=ckanext.eodp.dataset_plugin:DatasetPlugin
        eodp_organization=ckanext.eodp.organization_plugin:OrganizationPlugin
        eodp_api=ckanext.eodp.api_plugin:APIPlugin
        eodp_mqa=ckanext.eodp.mqa_plugin:MQAPlugin
        [paste.paster_command]
    	sparqlsync = ckanext.eodp.commands.sparql_sync:SparqlSync
    	translation = ckanext.eodp.commands.translation:Translation
    	util = ckanext.eodp.commands.edp_util:EdpUtil
    	statistics=ckanext.eodp.commands.statistics:Statistics
        [babel.extractors]
        ckan = ckan.lib.extract:extract_ckan
    ''',

)

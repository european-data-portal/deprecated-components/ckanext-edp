from ckanext.eodp.config import CONFIG
import ckan.plugins.toolkit as tk
import ckan.model as model
import ckan.logic as logic
import ckan.lib.base as base
import requests
import json
from helpers import language
from requests.auth import HTTPBasicAuth
import urllib
from ckan.common import _

import logging
log = logging.getLogger(__name__)

get_action = logic.get_action

from ckan.common import _
NotFound = logic.NotFound
abort = base.abort
NotAuthorized = logic.NotAuthorized
MQA_SIMILARITY_URL = CONFIG.get('mqa_service', 'similarity_url')
MQA_HTACCESS = CONFIG.getboolean('mqa_service', 'htaccess')
MQA_USER = CONFIG.get('mqa_service', 'user')
MQA_PASSWORD = CONFIG.get('mqa_service', 'password')


class MQAController(tk.BaseController):

    def read(self, id):

        context = {'model': model, 'session': model.Session,
                   'user': tk.c.user or tk.c.author, 'for_view': True,
                   'auth_user_obj': tk.c.userobj,
                   'request': tk.request}
        data_dict = {'id': id}
        try:
            tk.c.pkg_dict = tk.get_action('package_show')(context, data_dict)
            tk.c.pkg = context['package']
            tk.c.violations = get_violation_information(tk.c.pkg_dict)
            tk.c.mqa_link = get_mqa_link(tk.c.pkg_dict, context)
        except NotFound:
            abort(404, _('Dataset not found'))
        except NotAuthorized:
            abort(401, _('Unauthorized to read dataset %s') % id)

        return tk.render('package/mqa.html')

    def get_similar_datasets(self, id):
        context = {'model': model, 'session': model.Session,
                   'user': tk.c.user or tk.c.author, 'for_view': True,
                   'auth_user_obj': tk.c.userobj}
        data_dict = {'id': id}
        options = {}

        try:
            tk.c.pkg_dict = get_action('package_show')(context, data_dict)
            tk.c.pkg = context['package']
        except NotFound:
            abort(404, _('Dataset not found'))
        except NotAuthorized:
            abort(401, _('Unauthorized to read dataset %s') % id)

        if MQA_HTACCESS:
            options['auth'] = HTTPBasicAuth(MQA_USER, MQA_PASSWORD)

        url = MQA_SIMILARITY_URL + '/' + tk.c.pkg_dict['name'] + '?limit=10'

        try:
            response = requests.get(url, **options)
            if response.status_code == 200:
                package_list = json.loads(response.text)
                result = []
                for package in package_list:
                    try:
                        pkg_details = get_action('package_show')(context, {'id': package['id']})
                        result.append({
                            "id": pkg_details['id'],
                            "name": pkg_details['name'],
                            "title": pkg_details['title'],
                            "notes": pkg_details['notes'],
                            "dist": package['dist']
                        })
                    except NotFound as e:
                        log.error("Dataset not found")
                tk.c.packages = result
        except Exception as e:
            log.error(e.message)

        return tk.render('package/mqa_similarity.html')


def get_distribution_information(resource_id, mqa_result):
    if 'distributions' in mqa_result:
        for distribution in mqa_result['distributions']:
            if resource_id == distribution['instanceId']:
                return distribution

    return {'statusAccessUrl': 200}


def get_violation_information(pkg):
    result = get_mqa_result(pkg)

    if result and 'violations' in result:
        return result['violations']
    else:
        return None


def get_mqa_result(pkg):
    organization = False
    name = False
    if 'organization' in pkg and pkg['organization'] and 'name' in pkg['organization']:
        organization = pkg['organization']['name']

    if 'name' in pkg:
        name = pkg['name']

    if name:
        try:
            url = CONFIG.get('mqa_service', 'url') + "/api/dataset/" + pkg['name']
            response = requests.get(url, timeout=1.0)
            return json.loads(response.text)
        except requests.exceptions.Timeout:
            log.error("MQA Timeout for " + url)
        except:
            return None
    else:
        return None


def get_mqa_link(pkg, context):
    if 'organization' in pkg and pkg['organization'] and 'name' in pkg['organization']:
        return CONFIG.get('mqa_service', 'url') + "/" + language(context) + "/catalogue/" + pkg['organization']['name']
    else:
        return None

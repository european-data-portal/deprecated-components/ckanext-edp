import json

__author__ = 'fki'


def empty_string_to_none(value, context):
    if hasattr(value, 'strip') and not value.strip():
        return None
    else:
        return value


def convert_from_json(field):
    try:
        return json.dumps(field)
    except:
        return None


def convert_to_json(field):
    try:
        return json.loads(field)
    except ValueError:
        return field


def convert_list_to_nested_list(field):
    if isinstance(field, list):
        return {'list': field}
    else:
        return field


def null_to_empty_sting(field):
    if field is None:
        return ''
    else:
        return field


def convert_nested_list_to_json(field):
    try:
        return json.loads(field)['list']
    except (ValueError, TypeError):
        return field


def convert_list_to_extras(key, data, errors, context):

    # key = (id, index, key)
    new = True
    for k in data.keys():
        if data[k] == key[0]:
            old_data = data[('extras', k[1], 'value')]
            old_data = json.loads(old_data)
            index = key[1]
            if len(old_data) == index:
                old_data.append({key[-1]: data[key]})
            else:
                old_data[index][key[-1]] = data[key]
            data[('extras', k[1], 'value')] = json.dumps(old_data)
            new = False

    if new:
        current_indexes = [k[1] for k in data.keys()
                           if len(k) > 1 and k[0] == 'extras']

        new_index = max(current_indexes) + 1 if current_indexes else 0

        data[('extras', new_index, 'key')] = key[0]
        data[('extras', new_index, 'value')] = json.dumps([{key[-1]: data[key]}])








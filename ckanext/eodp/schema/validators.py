__author__ = 'fki'

import ckan.plugins.toolkit as tk
import pycountry
import json

_ = tk._
Invalid = tk.Invalid


def list_of_strings(value, context):
    if not isinstance(value, list):
        raise Invalid(_('Not a list'))
    if len(value) == 0:
        raise Invalid(_('List is empty'))
    for v in value:
        if not isinstance(v, basestring):
            raise Invalid('%s: %s' % (_('Not a string'), v))
    return value


def is_string(value, context):
    if not isinstance(value, basestring):
        raise Invalid(_('Not a string'))
    return value


def dictionary(value, context):
    if not isinstance(value, dict):
        raise Invalid(_('Not a dictionary'))
    if len(value) == 0:
        raise Invalid(_('Dictionary is empty'))
    return value


def list_of_dictionaries(value, context):
    if not isinstance(value, list):
        raise Invalid(_('Not a list'))
    if len(value) == 0:
        raise Invalid(_('List is empty'))
    for v in value:
        dictionary(v, context)
    return value


def validate_temporal_coverage(value, context):
    isodate = tk.get_validator('isodate')
    if not isinstance(value, dict):
        raise tk.Invalid("Wrong Data Type, Expected Dict")
    if 'start_date' not in value:
        raise tk.Invalid("Missing key start_date")
    if 'end_date' not in value:
        raise tk.Invalid("Missing key end_date")
    isodate(value['start_date'], context)
    isodate(value['end_date'], context)
    return value


def validate_org_spatial(value, context):
    try:
        list_of_strings(value, context)
        for v in value:
            try:
                pycountry.countries.get(alpha2=v).name
            except:
                raise tk.Invalid(v + ": Not a valid ISO 3166 Alpha-2 Country Code")
        return value
    except tk.Invalid as e:
        raise e




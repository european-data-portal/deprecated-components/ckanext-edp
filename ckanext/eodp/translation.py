import ckan.lib.helpers as h
import ckan.plugins.toolkit as tk
import pylons.config as ckan_config
import json
from babel.core import Locale
from babel.core import UnknownLocaleError
from ckanext.eodp.config import CONFIG
import logging
import gettext
import pycountry
import langdetect
from langdetect import DetectorFactory

DetectorFactory.seed = 0

log = logging.getLogger(__name__)

Invalid = tk.Invalid

config_key = 'translation'
AUTO_TRANS = CONFIG.getboolean(config_key, 'auto_translation')
LANGUAGES = CONFIG.getlist(config_key, 'languages')
FIELDS = CONFIG.getlist(config_key, 'fields')
DEFAULT = CONFIG.get(config_key, 'default_language')
TRANSFIELD = CONFIG.getoptional(config_key, 'translation_field_name', 'translation')
METAFIELD = TRANSFIELD + '_meta'
convert_package_name_or_id_to_id = tk.get_converter('convert_package_name_or_id_to_id')
TRANS_SERVICE_URL = CONFIG.get(config_key, 'service')
CALLBACK_URL = CONFIG.getoptional(config_key, 'callback_base', None)
API_KEY = CONFIG.get(config_key, 'api_key')


def _load_country_translations():
    locales = ckan_config.get('ckan.locales_offered', '').split()
    result = {}
    for l in locales:
        if l != 'en' and l != 'no':
            result[l] = gettext.translation('iso3166', pycountry.LOCALES_DIR, languages=[l])
        if l == 'no':
            result[l] = gettext.translation('iso3166', pycountry.LOCALES_DIR, languages=['nb'])

    return result

COUNTRIES_GETTEXT = _load_country_translations()

def get_country_translation(country):
    lang = get_language()
    if lang != 'en':
        return COUNTRIES_GETTEXT[lang].gettext(country)
    else:
        return country


def update(context, data_dict):
    '''
    :type data_dict: dict
    :param data_dict:
    :return:
    '''
    if 'id' in data_dict:
        ident = data_dict['id']
    else:
        ident = data_dict['name']
    old_package = tk.get_action('package_show')(context, {'id': convert_package_name_or_id_to_id(ident, context)})

    language = get_language()
    if language != DEFAULT:
        lang_dict = {}
        for field in FIELDS:
            lang_dict[field] = data_dict[field]
            data_dict[field] = old_package[field]

        if TRANSFIELD in old_package:
            translation = old_package[TRANSFIELD]
        else:
            translation = {}

        translation[language] = lang_dict
        data_dict[TRANSFIELD] = translation
    else:
        if TRANSFIELD in old_package:
            data_dict[TRANSFIELD] = old_package[TRANSFIELD]


def get(context, data_dict):
    '''
    :type data_dict: dict
    :param data_dict:
    :return:
    '''

    language = get_language()
    # ToDo Check if API Request
    if language != DEFAULT:
        try:
            lang_dict = data_dict[TRANSFIELD][language]
            for field in FIELDS:
                if field in lang_dict:
                    data_dict[field] = lang_dict[field]
        except KeyError:
            pass


def translate_field(field, data_dict):
    '''
    :type field: str
    :type data_dict: dict
    :param field:
    :return:
    '''

    language = get_language()
    if language != DEFAULT and language in LANGUAGES:
        try:
            lang_dict = data_dict[TRANSFIELD][language]
            if field in lang_dict:
                return lang_dict[field]
            else:
                return data_dict[field]
        except KeyError:
            if field in data_dict:
                return data_dict[field]
            else:
                return None
    else:
        if field in data_dict:
            return data_dict[field]
        else:
            return None


def index(data_dict):
    '''
    :type data_dict: dict
    :param data_dict:
    :return:
    '''

    complete_dict = json.loads(data_dict['data_dict'])

    if 'resources' in complete_dict:
        resources = complete_dict['resources']
        resource_translation = {}
        for resource in resources:
            if TRANSFIELD in resource:
                resource_trans = json.loads(resource[TRANSFIELD])
                for lang in resource_trans:
                    for field in resource_trans[lang]:
                        if field == 'name':
                            name_key = _build_language_solr_field('res_name', lang)
                            if name_key in resource_translation:
                                resource_translation[name_key].append(resource_trans[lang][field])
                            else:
                                resource_translation[name_key] = [resource_trans[lang][field]]
                        if field == 'description':
                            name_key = _build_language_solr_field('res_description', lang)
                            if name_key in resource_translation:
                                resource_translation[name_key].append(resource_trans[lang][field])
                            else:
                                resource_translation[name_key] = [resource_trans[lang][field]]

        if resource_translation:
            data_dict.update(resource_translation)

    if TRANSFIELD in data_dict:
        translation_dict = data_dict[TRANSFIELD]

        complete_dict = json.loads(data_dict['data_dict'])

        # Delete language extra field from index
        del data_dict[TRANSFIELD]
        del data_dict[_build_extras_solr_field(TRANSFIELD)]

        # Transform into dict, if not possible return pkg_dict
        try:
            translation_dict = json.loads(translation_dict)
        except ValueError as e:
            log.debug(e.message)
            return data_dict

        # Iterate through translation dict
        for key in translation_dict:
            if key in LANGUAGES:
                language_dict = _create_translation_package(translation_dict[key], key)
                data_dict.update(language_dict)

    return data_dict


def _build_extras_solr_field(field):
    return 'extras_' + field


def _build_language_solr_field(field, language):
    return field + '_' + language


def _create_translation_package(language_dict, language):
    pkg = {}
    for key in language_dict:
        if key in FIELDS:
            pkg[_build_language_solr_field(key, language)] = language_dict[key]

    return pkg


def get_language_query(search_params, lang_detection=False):
    params = tk.request.params
    langs = []
    for p in params.items():
        if p[0] == 'ext_langs':
            langs.append(p[1])

    if not langs:
        langs = [DEFAULT] + LANGUAGES

    if 'q' in search_params:
        q = search_params['q']
    else:
        q = ''
    query_template = 'title%s^4 text%s '
    query = ''

    if lang_detection and q:
        detected_langs = _detect_languages(q)
        if detected_langs:
            for detected_lang in detected_langs:
                detected_lang = '_' + detected_lang
                query += query_template % (detected_lang, detected_lang)
    else:
        for lang in langs:
            if lang == DEFAULT:
                query += query_template % ('', '')
            else:
                lang_ident = '_' + lang
                query += query_template % (lang_ident, lang_ident)

    query += 'title^4 text name^4 tags^2 groups^2'
    return query


def _detect_languages(q):
    result = []
    try:
        detected_langs = langdetect.detect_langs(q.lower())
        for lang in detected_langs:
            if lang.prob > 0.1 and lang.lang in LANGUAGES:
                result.append(lang.lang)
    except Exception:
        pass
    return result


def get_language_selection():
    request = tk.request
    params = request.GET
    languages = []
    for p in params.items():
        if p[0] == 'ext_langs':
            languages.append(p[1])

    if not languages:
        languages.append(get_language())

    return languages


def merge_dicts(*dicts):
    result_dict = {}
    for d in dicts:
        result_dict.update(d)
    return result_dict


def get_language():
    try:
        lang = h.lang()
        return lang
    except TypeError:
        return DEFAULT


def translate(field, data_dict):
    '''
    :type field: str
    :type data_dict: dict
    :param field:
    :param data_dict:
    :return:
    '''
    return translate_field(field, data_dict)


def is_machine_translated(data_dict):
    # ToDo Return more translation info

    try:
        meta = data_dict['translation_meta']
    except KeyError:
        return False
    language = get_language()

    try:
        lang_meta = meta[language]
    except KeyError:
        return False

    tag = lang_meta['tag']
    if 'mtec' in tag:
        if 'status' in meta:
            status = meta['status']
            tag = tag.split('-')
            original_language = tag[2]
            original_code = tag[2]
            target_language = tag[0]
            try:
                if language == 'no':
                    original_language = Locale(original_language).get_display_name(locale='nb')
                else:
                    original_language = Locale(original_language).get_display_name(locale=language)
            except UnknownLocaleError:
                pass
            try:
                if language == 'no':
                    target_language = Locale(target_language).get_display_name(locale='nb')
                else:
                    target_language = Locale(target_language).get_display_name(locale=language)
            except UnknownLocaleError:
                pass
            if status == 'finished':
                return {
                    'tag': 'mtec',
                    'original_code': original_code,
                    'from': original_language,
                    'to': target_language
                }
            elif status == 'processing':
                return {
                    'tag': 'processing',
                    'original_code': original_code,
                    'from': original_language,
                    'to': target_language
                }
            elif status == 'updating':
                return {
                    'tag': 'updating',
                    'original_code': original_code,
                    'from': original_language,
                    'to': target_language
                }
            else:
                return False
        else:
            return False
    else:
        return False


def translation_is_available(data_dict):
    lang = get_language()
    translations = get_available_translations(data_dict)
    if lang in translations:
        return {
            'status': True
        }
    else:
        if METAFIELD in data_dict:
            try:
                default = data_dict[METAFIELD]['default'].lower()
            except KeyError:
                default = DEFAULT

            try:
                if lang == 'no':
                    lang = 'nb'
                default_value = Locale(default).get_display_name(locale=lang)
            except UnknownLocaleError:
                default_value = default

        else:
            try:
                if lang == 'no':
                    lang = 'nb'
                default_value = Locale(DEFAULT).get_display_name(locale=lang)
            except UnknownLocaleError:
                default_value = DEFAULT

        return {
            'status': False,
            'default': default_value
        }



def get_available_translations(data_dict):
    if METAFIELD in data_dict:
        return data_dict[METAFIELD]['languages']
    else:
        return [DEFAULT]


def get_available_languages():
    locales = ckan_config.get('ckan.locales_offered', '').split()
    return locales


def get_display_name_for_locale(locale):
    language = get_language()
    name = Locale(locale).get_display_name(locale=language)
    return name


__author__ = 'fki'

from ckanext.eodp.config import CONFIG
import ckan.plugins.toolkit as tk
from ckan.lib import helpers

import logging
log = logging.getLogger(__name__)

formats = CONFIG.getlist('visualisation', 'formats')


class VisualisationController(tk.BaseController):

    def main(self, id):
        from pylons import c

        c.server_url = CONFIG.get('visualisation', 'server_url')
        c.client_url = CONFIG.get('visualisation', 'client_url')
        context = {}
        data_dict = {'id': id}
        try:
            c.resource = tk.get_action('resource_show')(context, data_dict)
        except tk.ObjectNotFound:
            tk.abort(404, tk._('Resource not found'))

        return tk.render('visualisation/main.html')


def get_visualisation_data(resource):
    """
    :type resource: dict
    :rtype: dict
    :param res:
    :return:
    """
    format_name = resource['format'].lower()
    if format_name in formats:
        if resource['name'] == 'EDP-Statistics':
            return{
                'link': helpers.url_for('statistics_current_selection'),
            }
        return {
            'link': _build_visu_link(resource['id']),
        }
    return None


def pkg_visualisation_enabled(data_dict):
    resources = data_dict['resources']
    for r in resources:
        if r['format'].lower() in formats:
            return True
    return False


def _build_visu_link(id):
    return helpers.url_for('resource_visualisation', id=id)


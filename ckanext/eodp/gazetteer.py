__author__ = 'fki'

import ckan.plugins.toolkit as tk
import json
import requests
from pygeoif import geometry
from requests.exceptions import RequestException
from config import CONFIG
from ckanext.eodp.translation import get_language

import logging
log = logging.getLogger(__name__)

API_URL = CONFIG.get('gazetteer', 'api_url')


class GazetteerController(tk.BaseController):

    def autocomplete(self):
        tk.response.headers['Content-Type'] = 'application/json;charset=utf-8'
        request = tk.request
        query = request.params.get('q', None)
        # result = {
        #     'options': ['Hallo', 'test'],
        #     'geometry': ['48.85341 2.3488', '666.6 666.6']
        # }
        # return json.dumps(result)

        if query:
            try:
                result = _query_suggestions(query)
            except RequestException:
                # ToDo Set this properly
                tk.response.status_int = 200
                return json.dumps({'error': 'Gazetteer Service does not respond.'})
        else:
            result = {}

        tk.response.headers['Content-Type'] = 'application/json;charset=utf-8'
        return json.dumps(result)


def _query_suggestions(query):
    url = API_URL + '?q=' + query
    try:
        results = requests.get(url, timeout=1)
    except:
        raise RequestException
    lang = get_language()
    log.info(lang)
    results = json.loads(results.text)
    try:
        items = results['response']['docs']
    except KeyError:
        return {}

    lang_pattern = '%s_%s'
    # name_ident = lang_pattern % ('name', lang)
    # admunit1_ident = lang_pattern % ('admunit1_name', lang)
    # admunit2_ident = lang_pattern % ('admunit2_name', lang)
    #

    result = []
    for item in items:
        valid = True
        location = {}
        text = []
        if 'name' in item:
            text.append(item['name'])
        if 'admunit1_name' in item:
            text.append(item['admunit1_name'])
        if 'admunit2_name' in item:
            text.append(item['admunit2_name'])

        if len(text) > 0:
            location['name'] = ', '.join(text)
        else:
            valid = False

        if 'featureType' in item:
            location['featureType'] = item['featureType']
        else:
            valid = False

        if 'geometry' in item:
            box = _wkt_to_bounding_box(item['geometry'][0], item['featureType'])
            if box:
                location['geometry'] = box
            else:
                valid = False
        else:
            valid = False

        if valid:
            result.append(location)

    return result

def _wkt_to_bounding_box(value, feature_type):

    # Get a geometry object from WKT
    try:
        result = geometry.from_wkt(value)
    except NotImplementedError:
        return None

    # Transform the geometry to a bounding box
    if type(result) is geometry.Polygon:
        box = result.bounds
        return _point_to_str(box)

    elif type(result) is geometry.MultiPolygon:
        box = result.bounds
        return _point_to_str(box)

    elif type(result) is geometry.GeometryCollection:
        box = result.bounds
        return _point_to_str(box)

    elif type(result) is geometry.Point:
        box = _bounding_box_for_point(result, feature_type)
        return _point_to_str(box)

    else:
        return None


def _bounding_box_for_point(point, featue_type):
    x = point.x
    y = point.y
    if featue_type == 'PPL':
        extend_x = 0.25
    elif featue_type == 'ADM1':
        extend_x = 0.5
    elif featue_type == 'ADM2':
        extend_x = 0.25
    else:
        extend_x = 0.25
    extend_y = extend_x * 2
    x1 = y - extend_y
    x2 = x - extend_x
    y1 = y + extend_y
    y2 = x + extend_x
    return x1, x2, y1, y2


def _point_to_str(point):
    return ','.join(str(i) for i in point)







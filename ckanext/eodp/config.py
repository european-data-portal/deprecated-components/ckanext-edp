import ConfigParser
import os
from ConfigParser import NoOptionError, NoSectionError

import logging
log = logging.getLogger(__name__)


class CustomConfigParser(ConfigParser.ConfigParser):

    def getlist(self, section, option):
        value = self.get(section, option)
        return value.split(' ')

    def getoptional(self, section, option, default):
        try:
            result = self.get(section, option)
            return result
        except (NoOptionError, NoSectionError):
            return default


CONFIG = CustomConfigParser()
config_file = os.path.dirname(__file__)
config_file = os.path.join(config_file, '../..', 'config.ini')
config_file = os.path.abspath(config_file)
result = CONFIG.read(config_file)

if result:
    log.info("Loaded config.ini")
else:
    log.error("No config.ini file was found.")

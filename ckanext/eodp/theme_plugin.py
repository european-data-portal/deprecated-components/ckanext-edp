__author__ = 'fki'

import ckan.plugins as plugins
import ckan.plugins.toolkit as tk
import ConfigParser

import logging
log = logging.getLogger(__name__)


class ThemePlugin(plugins.SingletonPlugin):

    plugins.implements(plugins.IConfigurer)
    log.info("Theme Plugin loaded")

    def update_config(self, config):

        config['ckan.site_title'] = "European Data Portal"
        tk.add_template_directory(config, 'templates')
        tk.add_public_directory(config, 'public')
        tk.add_resource('fanstatic', 'edp_theme')
        tk.add_resource('../../node_modules/chart.js/dist', 'chart_js')

from ckan.lib import helpers


def package_to_schema(pkg):
    r = {
        "@context": "http://schema.org/",
        "@type": "Dataset"
    }

    if 'title' in pkg:
        r['name'] = pkg['title']

    if 'notes' in pkg:
        r['description'] = pkg['notes']

    if 'name' in pkg:
        r['url'] = helpers.url_for(controller='package', action='read', id=pkg['name'], qualified=True)

    if 'tags' in pkg and pkg['tags']:
        r['keywords'] = []
        for tag in pkg['tags']:
            r['keywords'].append(tag['name'])

    if 'url' in pkg and pkg['url'] and helpers.is_url(pkg['url']):
        r['sameAs'] = pkg['url']

    if 'original_source' in pkg and pkg['original_source'] and helpers.is_url(pkg['original_source']):
        r['sameAs'] = pkg['original_source']

    if 'resources' in pkg and pkg['resources']:
        r['distribution'] = []
        for resource in pkg['resources']:
            r['distribution'].append(resource_to_schema(resource))

    return r


def resource_to_schema(resource):
    r = {
        "@type": "DataDownload"
    }

    if 'url' in resource:
        r['contentUrl'] = resource['url']

    if 'format' in resource:
        r['encodingFormat'] = resource['format']

    return r









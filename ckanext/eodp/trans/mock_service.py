import ckan.plugins.toolkit as tk
from multiprocessing import Process
import json
import logging
import requests
log = logging.getLogger(__name__)


def _callback(pkg):
    trans = {}
    import json

    api_key = pkg['callback']['headers']['Authorization']
    dataset_id = pkg['callback']['payload']['id']
    url = pkg['callback']['url']
    langs = pkg['languages']
    pkg_dict = pkg['data_dict']
    original = pkg['original_language']

    for l in langs:
        trans[l] = {}
        for key in pkg_dict:
            trans[l][key] = pkg_dict[key] + ' in ' + l

    payload = {
        'id': dataset_id,
        'translation': trans,
        'originalLanguage': original
    }
    headers = {
        'Content-type': 'application/json',
        'Authorization': api_key
    }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    log.info(response.status_code)
    log.info('Called Mock for Dataset ' + str(payload))


def process(pkg):
    import time
    log.info("Started Translation Process")
    time.sleep(5)
    _callback(pkg)


class MockTranslationService(tk.BaseController):

    def translate(self):
        request = tk.request
        if request.method != 'POST':
            raise tk.ObjectNotFound
        body = json.loads(request.body)
        Process(target=process, args=(body,)).start()
        return "Translation Request Successful"
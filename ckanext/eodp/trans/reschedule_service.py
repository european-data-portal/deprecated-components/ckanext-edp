import ckan.plugins.toolkit as tk
from ckan import model
import os
import json
import datetime
import dateutil.parser
from ckanext.eodp.trans import auto_translation
from ckanext.eodp.trans import *
from ckan.controllers.api import ApiController
import logging
log = logging.getLogger(__name__)


class TranslationService(ApiController):

    def _write_log(self, message):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        path = os.path.join(dir_path, 'reschedule_log')
        log_file = open(path, 'a')
        log_file.write(message + '\n')
        log_file.close()

    def reschedule_trans(self):


        request = tk.request
        if request.method != 'POST':
            return self._finish_bad_request("Method not supported")

        try:
            body = json.loads(request.body)
        except ValueError:
            return self._finish_bad_request("No body provided")

        try:
            pkg_name = body['name']
        except KeyError:
            return self._finish_bad_request("Please provide the package name in the JSON body")

        log.info('Rescheduling of "%s"' % pkg_name)
        context = {'model': model, 'session': model.Session, 'ignore_auth': True}
        pkg = tk.get_action('package_show')(context, {"id": pkg_name})

        now = datetime.datetime.now().isoformat()
        if METAFIELD in pkg:
            metafield = pkg[METAFIELD]
            languages, original_language = auto_translation._get_target_languages_for_trans_service(pkg)
            if languages:
                for l in languages:
                    metafield[l]['issued'] = now
                try:
                    pkg_result = tk.get_action('package_patch')(context, pkg)
                    auto_translation.request_translation(context, pkg)
                    return self._finish_ok('Dataset %s rescheduled' % pkg['name'])
                except Exception as e:
                    log.error(e)
                    # self._write_log('Could not patch package [%s]' % pkg['name'])
                    return self._finish_bad_request('Could not patch package ' + str(e))
            else:
                return self._finish_bad_request('Cannot determine target languages.')

        return self._finish_ok('')

__author__ = 'fki'

from ckanext.eodp.trans import *
import ckan.plugins.toolkit as tk

import requests
import json
import datetime
from collections import Set

import logging
log = logging.getLogger(__name__)


def now():
    return datetime.datetime.now().isoformat()


def before_package_create(data_dict):
    return _before_package_create_or_update(data_dict)


def before_package_update(data_dict, context):
    result = _before_package_create_or_update(data_dict, update=True)

    if not result:
        return False

    if 'id' in data_dict:
        ident = data_dict['id']
    else:
        ident = data_dict['name']
    old_package = tk.get_action('package_show')(context, {'id': convert_package_name_or_id_to_id(ident, context)})

    def translation_switch(new_package, old_pkg, fields, default):
        new_package[TRANSFIELD][default] = {}
        for field in fields:
            if field in old_pkg:
                try:
                    temp = new_package[field]
                    new_package[field] = old_pkg[field]
                    new_package[TRANSFIELD][default][field] = temp
                except KeyError:
                    pass

        pass

    if TRANSFIELD in old_package:
        data_dict[TRANSFIELD] = old_package[TRANSFIELD]
        do_switch = False
        if METAFIELD in data_dict and data_dict[METAFIELD]['default'] != DEFAULT:
            if METAFIELD in old_package and old_package[METAFIELD]['default'] == DEFAULT:
                do_switch = True

        if 'resources' in data_dict:
            for r in data_dict['resources']:
                for old_r in old_package['resources']:
                    if 'id' in r and r['id'] == old_r['id'] and TRANSFIELD in old_r:
                        r[TRANSFIELD] = old_r[TRANSFIELD]
                        if do_switch:
                            translation_switch(r, old_r, ['name', 'description'], data_dict[METAFIELD]['default'])

        if do_switch:
            translation_switch(data_dict, old_package, FIELDS, data_dict[METAFIELD]['default'])
            data_dict[METAFIELD]['default'] = DEFAULT

        data_dict[METAFIELD]['status'] = 'updating'
        for key, value in data_dict[TRANSFIELD].iteritems():
            if key in data_dict[METAFIELD]:
                if key not in data_dict[METAFIELD]['languages']:
                    data_dict[METAFIELD]['languages'].append(key)

        if DEFAULT in data_dict[METAFIELD] and DEFAULT not in data_dict[METAFIELD]['languages']:
            data_dict[METAFIELD]['languages'].append(DEFAULT)

    return result


def _before_package_create_or_update(data_dict, update=False):
    """
    This method should be called before the package_create action to
    prepare the package for translation
    :param data_dict:
    :return: True when a translation request if required
    """

    # If there is no translation meta data set it to default
    if METAFIELD not in data_dict:
        data_dict[METAFIELD] = {
            'default': DEFAULT
        }
        base_language = DEFAULT
    else:
        base_language = get_base_language(data_dict)

    if base_language not in SUPPORTED_LANGUAGES:
        _set_one_language(base_language, data_dict)
        return False

    if 'auto_translation' in data_dict[METAFIELD] and \
            data_dict[METAFIELD]['auto_translation'] is False:
        del data_dict[METAFIELD]['auto_translation']
        if not update:
            _set_one_language(base_language, data_dict)
        return False

    if update:
        data_dict['state'] = 'active'
    else:
        data_dict['state'] = 'active'
        #data_dict['state'] = 'draft'

    # Set the meta data for every language, when not set
    if TRANSFIELD in data_dict:

        if 'resources' in data_dict:
            for resource in data_dict['resources']:
                if TRANSFIELD in resource:
                    if DEFAULT in resource[TRANSFIELD]:
                        for field in resource[TRANSFIELD][DEFAULT]:
                            if field in ['description', 'name'] and field in resource:
                                temp = resource[field]
                                resource[field] = resource[TRANSFIELD][DEFAULT][field]
                                resource[TRANSFIELD][DEFAULT][field] = temp
                        resource[TRANSFIELD][base_language] = resource[TRANSFIELD].pop(DEFAULT)

        # Set to store all available languages
        languages = set()

        # When the default language is provided in translation field
        # switch it to the base fields
        if DEFAULT in data_dict[TRANSFIELD]:
            for field in data_dict[TRANSFIELD][DEFAULT]:
                if field in FIELDS and field in data_dict:
                    temp = data_dict[field]
                    data_dict[field] = data_dict[TRANSFIELD][DEFAULT][field]
                    data_dict[TRANSFIELD][DEFAULT][field] = temp

            data_dict[TRANSFIELD][base_language] = data_dict[TRANSFIELD].pop(DEFAULT)
            data_dict[METAFIELD]['default'] = DEFAULT
            base_language = DEFAULT

        for lang in data_dict[TRANSFIELD]:
            languages.add(lang)
            data_dict[METAFIELD][lang] = {
                'tag': lang,
                'received': now()
            }

        if base_language not in data_dict[TRANSFIELD]:
            languages.add(base_language)
            data_dict[METAFIELD][base_language] = {
                'tag': base_language,
                'received': now()
            }

        data_dict[METAFIELD]['languages'] = list(languages)

    else:
        data_dict[METAFIELD]['languages'] = [base_language]
        data_dict[METAFIELD][base_language] = {
            'tag': base_language,
            'received': now()
        }

    if AUTO_TRANS:
        target_languages = _get_target_languages(data_dict)
        if not target_languages:
            data_dict['state'] = 'active'
            data_dict['translated'] = 'true'
        else:
            data_dict[METAFIELD]['status'] = 'processing'
            for lang in target_languages:
                data_dict[METAFIELD][lang] = {
                    'tag': '%s-t-%s-t0-mtec' % (lang, base_language),
                    'issued': now()
                }
    else:
        data_dict['state'] = 'active'
    return True


def after_package_create(context, data_dict):
    if AUTO_TRANS:
        _request_translation(context, data_dict)


def request_translation(context, data_dict):
    if AUTO_TRANS:
        _request_translation(context, data_dict)


def _set_one_language(language, data_dict):
    data_dict['state'] = 'active'
    data_dict[METAFIELD][language] = {
        'tag': language,
        'received': now()
    }
    data_dict[METAFIELD]['languages'] = [language]


def _request_translation(context, data_dict):
    """
    :type data_dict: dict
    :param data_dict: The package
    """
    if CALLBACK_URL:
        callback_url = CALLBACK_URL
    else:
        # Todo Make this better
        callback_url = 'http://localhost:5000'

    base_language = get_base_language(data_dict)

    trans_dict = {}
    target_languages, original_language = _get_target_languages_for_trans_service(data_dict)

    if not target_languages:
        return

    if original_language == DEFAULT or base_language == original_language:
        for field in FIELDS:
            trans_dict[field] = data_dict[field]
    else:
        for field in FIELDS:
            trans_dict[field] = data_dict[TRANSFIELD][original_language][field]

    resource_dict = _get_resources_trans_dict(data_dict, original_language, base_language)
    trans_dict.update(resource_dict)

    payload = {}
    payload['callback'] = {
        'url': callback_url + '/api/3/action/package_translate',
        'method': 'POST',
        'payload': {
            'id': data_dict['id']
        },
        'headers': {
            'Authorization': API_KEY
        }
    }
    payload['languages'] = target_languages
    payload['original_language'] = original_language
    payload['data_dict'] = trans_dict

    #log.debug('Translation Request: %s' % json.dumps(payload))
    headers = {'Content-type': 'application/json'}

    def _set_abort_meta_data():
        if METAFIELD in data_dict:
            meta = data_dict[METAFIELD]
        else:
            meta = {}

        meta['status'] = 'aborted'
        data_dict[METAFIELD] = meta
        tk.get_action('package_patch')(context, {'id': data_dict['id'], METAFIELD: meta})

    try:
        response = requests.post(TRANS_SERVICE_URL, data=json.dumps(payload), headers=headers, timeout=2)
        #log.info('TRANSLATION REQUEST - %s' % payload)
        if not str(response.status_code).startswith('2'):
            _set_abort_meta_data()
            log.error('Request Error. Response: %d - %s' % (response.status_code, response.reason))
    except (requests.Timeout, requests.ConnectionError) as e:
        _set_abort_meta_data()
        log.error(e.message)


def _get_target_languages(data_dict):
    languages = data_dict[METAFIELD]['languages']
    return [i for i in SUPPORTED_LANGUAGES if i not in languages]


def _get_target_languages_for_trans_service(data_dict):
    meta = data_dict[METAFIELD]
    result = []
    original_language = False
    for supported in SUPPORTED_LANGUAGES:
        if supported in meta:
            if 'tag' in meta[supported] and 'mtec' in meta[supported]['tag']:
                if not original_language:
                    tag = meta[supported]['tag']
                    tag = tag.split('-')
                    original_language = tag[2]
                result.append(supported)
    return result, original_language


def _get_resources_trans_dict(data_dict, original_language, base_language):
    result = {}
    for r in data_dict['resources']:
        r_id = r['id']
        if 'name' in r and r['name']:
            if original_language == DEFAULT or base_language == original_language or TRANSFIELD not in r or not r[TRANSFIELD]:
                result[r_id + '_' + 'name'] = r['name']
            else:
                try:
                    result[r_id + '_' + 'name'] = r[TRANSFIELD][original_language]['name']
                except KeyError:
                    pass

        if 'description' in r and r['description']:
            if original_language == DEFAULT or base_language == original_language or TRANSFIELD not in r or not r[TRANSFIELD]:
                result[r_id + '_' + 'description'] = r['description']
            else:
                try:
                    result[r_id + '_' + 'description'] = r[TRANSFIELD][original_language]['description']
                except KeyError:
                    pass

    return result


def get_base_language(data_dict):
    if 'default' in data_dict[METAFIELD]:
        base_language = data_dict[METAFIELD]['default']
    else:
        base_language = DEFAULT
    return base_language


def set_base_language(data_dict, language):
    if METAFIELD not in data_dict:
        return
    data_dict[METAFIELD]['default'] = language


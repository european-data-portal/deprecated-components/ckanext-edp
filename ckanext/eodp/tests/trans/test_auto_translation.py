__author__ = 'fki'

from nose.tools import assert_in, assert_equals, assert_not_in
from ckan.new_tests.factories import Sysadmin, Dataset
from ckan.new_tests.helpers import FunctionalTestBase, call_action
from ckanext.eodp.trans import *

class TestAutoTranslation(FunctionalTestBase):

    def setup(self):
        super(TestAutoTranslation, self).setup()
        self.context = {'api_version': 3}

    def test_auto_creation_translation_meta_just_en(self):
        """
        Tests the automatic creation of the translation metadata,
        if no translation data is provided
        :return:
        """
        dataset = call_action('package_create',
                              context=self.context,
                              title='Test Dataset',
                              name='test-dataset'
                            )

        assert_not_in(TRANSFIELD, dataset)
        assert_in('translation_meta', dataset)
        meta = dataset['translation_meta']
        assert_in('languages', meta)
        assert_equals(meta['languages'], ['en'])
        for l in LANGUAGES:
            assert_in(l, meta)
            assert_equals(meta[l]['tag'], 'processing')
        assert_in(DEFAULT, meta)
        assert_in('default', meta)
        assert_equals(meta['default'], DEFAULT)

    def test_auto_translation_result_just_en(self):

        dataset = call_action('package_create',
                              context=self.context,
                              title='Test Dataset',
                              name='test-dataset'
                              )

        # ToDo Mock Translation Service Properly






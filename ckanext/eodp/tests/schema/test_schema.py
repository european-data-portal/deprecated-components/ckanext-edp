__author__ = 'fki'

from nose.tools import assert_true
from ckan.new_tests.factories import Sysadmin, Dataset
from ckan.new_tests.helpers import FunctionalTestBase, call_action

class TestSchema(FunctionalTestBase):

    def test_dataset_schema(self):
        user = Sysadmin()

        dataset = call_action('package_create',
                              title='Test Dataset',
                              name='test-dataset'
                              )

import ckan.plugins.toolkit as tk
from ckan.controllers.feed import FeedController
from ckan.controllers.feed import _package_search
from ckan.controllers.feed import _create_atom_id
from ckan.controllers.feed import _FixedAtom1Feed
from ckanext.eodp import translation
import ckan.lib.helpers as h
from ckan.common import g
from pylons import config
import json
import webhelpers

ITEMS_LIMIT = 20
request = tk.request
response = tk.response


class CustomFeedController(FeedController):

    def custom(self):
        q = request.params.get('q', u'')
        fq = ''
        search_params = {}
        search_extras = {}
        for (param, value) in request.params.items():
            if param not in ['q', 'page', 'sort'] and len(value) and not param.startswith('_'):
                if not param.startswith('ext_'):
                    search_params[param] = value
                    fq += ' %s:"%s"' % (param, value)
                else:
                    search_extras[param] = value

        page = self._get_page_number(request.params)

        limit = ITEMS_LIMIT
        data_dict = {
            'q': q,
            'fq': fq,
            'start': (page - 1) * limit,
            'rows': limit,
            'sort': request.params.get('sort', 'metadata_modified desc'),
            'extras': search_extras
        }

        item_count, results = _package_search(data_dict)

        navigation_urls = self._navigation_urls(request.params,
                                                item_count=item_count,
                                                limit=data_dict['rows'],
                                                controller='feed',
                                                action='custom')

        feed_url = self._feed_url(request.params,
                                  controller='feed',
                                  action='custom')

        atom_url = h._url_with_params('/feeds/custom.atom',
                                      search_params.items())

        alternate_url = self._alternate_url(request.params)

        return self.output_feed(results,
                                feed_title=u'%s - Atom Feed' % g.site_title,
                                feed_description=None,
                                feed_link=alternate_url,
                                feed_guid=_create_atom_id(atom_url),
                                feed_url=feed_url,
                                navigation_urls=navigation_urls)

    def output_feed(self, results, feed_title, feed_description,
                    feed_link, feed_url, navigation_urls, feed_guid):
        author_name = config.get('ckan.feeds.author_name', '').strip() or \
                      config.get('ckan.site_id', '').strip()

        author_link = config.get('ckan.feeds.author_link', '').strip() or \
                      config.get('ckan.site_url', '').strip()  # TODO language
        feed = _FixedAtom1Feed(
            title=feed_title,
            link=feed_link,
            description=feed_description,
            language=u'en',
            author_name=author_name,
            author_link=author_link,
            feed_guid=feed_guid,
            feed_url=feed_url,
            previous_page=navigation_urls['previous'],
            next_page=navigation_urls['next'],
            first_page=navigation_urls['first'],
            last_page=navigation_urls['last'],
        )

        for pkg in results:
            feed.add_item(
                title=translation.translate('title', pkg),
                link=self.base_url + h.url_for(controller='package',
                                               action='read',
                                               id=pkg['id']),
                description=translation.translate('notes', pkg),
                updated=h.date_str_to_datetime(pkg.get('metadata_modified')),
                published=h.date_str_to_datetime(pkg.get('metadata_created')),
                unique_id=_create_atom_id(u'/dataset/%s' % pkg['id']),
                author_name=pkg.get('author', ''),
                author_email=pkg.get('author_email', ''),
                categories=[t['name'] for t in pkg.get('tags', [])],
                enclosure=webhelpers.feedgenerator.Enclosure(
                    self.base_url + h.url_for(controller='api',
                                              register='package',
                                              action='show',
                                              id=pkg['name'],
                                              ver='2'),
                    unicode(len(json.dumps(pkg))),  # TODO fix this
                    u'application/json')
            )
        response.content_type = feed.mime_type
        return feed.writeString('utf-8')

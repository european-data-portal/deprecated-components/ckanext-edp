"use strict";

ckan.module('edp_geo', function ($, _) {
    return {
        elements: {
            form: $('.search-form'),
            geo_input: "ext_geo_input"
        },
        initialize: function () {
            var _this = this;
            this._init();

            var result = null;
            var gazetteer_url = this.options.url;

            this.el.typeahead({
                items: 20,
                minLength: 3,
                matcher: function (item) {
                    return true;
                },
                source: function (query, process) {
                    return $.get(gazetteer_url, { q: query }, function (data) {
                        result = data;
                        var options = [];
                        for(var i=0; i<data.length; i++){
                            options.push(data[i].name)
                        }
                        return process(options);
                    });
                },
                updater: function (item) {
                    for(var i=0; i<result.length; i++){
                        if(result[i].name == item) {
                            $('#ext_bbox').val(result[i].geometry);
                            $('#ext_prev_extent').val("");
                            $("#" + _this.elements.geo_input).val(result[i].name);
                            _this.elements.form.submit();
                            return item;
                        }
                    }
                }
            });
        },
        _init: function () {
            this._tweakTypeahead();
            this.el.val(this._getParameterByName(this.elements.geo_input));
            var _this = this;
            $([this.elements.geo_input]).each(function(index, item){
                if ($("#" + item).length === 0) {
                    $('<input type="hidden" />').attr({'id': item, 'name': item}).appendTo(_this.elements.form);
                }
            });
        },
        _tweakTypeahead: function () {
            $.fn.typeahead.Constructor.prototype.process = function (items) {
                var that = this;
                items = $.grep(items, function (item) {
                    return that.matcher(item)
                });
                items = this.sorter(items);
                if (!items.length) {
                    return this.shown ? this : this
                }
                return this.render(items.slice(0, this.options.items)).show()
            };
        },
        _getParameterByName: function (name) {
            var match = RegExp('[?&]' + name + '=([^&]*)')
                .exec(window.location.search);
            return match ?
                decodeURIComponent(match[1].replace(/\+/g, ' '))
                : null;
        }

    }
});
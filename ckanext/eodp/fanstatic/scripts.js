"use strict";

$(document).ready(function(){
    $('#language-search-selection').multiselect({
        includeSelectAllOption: true,
        numberDisplayed: 3
    });
    $('.icon-tooltip').tooltip();
    $('.btn-tooltip').tooltip({container: 'body'});

    // Shoe More Facets
    var $facets = $('.edp-facet');
    var $moreButton = $('.edp-facet .edp-more-facets');
    var $lessButton = $('.edp-facet .edp-less-facets');
    var STORAGEID = 'facet';
    var buildKey = function (key) {
        return STORAGEID + '_' + key;
    };

    $moreButton.click(function(elm){
        $(this).closest('.edp-facet').find('.edp-hidden-facet').slideDown();
        $(this).closest('.edp-facet').find('.edp-basic-facet li').last().css('border-bottom', '1px dotted #dddddd');
        $(this).next('.edp-less-facets').show();
        $(this).hide();
        sessionStorage.setItem(buildKey($(this).closest('.edp-facet').data('name')), 'true');
        return false;
    });

    $lessButton.click(function(elm){
        $(this).closest('.edp-facet').find('.edp-hidden-facet').slideUp();
        $(this).closest('.edp-facet').find('.edp-basic-facet li').last().css('border-bottom', '0 none');
        $(this).prev('.edp-more-facets').show();
        $(this).hide();
        sessionStorage.removeItem(buildKey($(this).closest('.edp-facet').data('name')));
        return false;
    });

    var resetFacets = function () {
        sessionStorage.clear();
    };
    var path = window.location.href;

    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function(searchString, position) {
          var subjectString = this.toString();
          if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
          }
          position -= searchString.length;
          var lastIndex = subjectString.indexOf(searchString, position);
          return lastIndex !== -1 && lastIndex === position;
        };
    }

    if(path.endsWith('dataset')) {
        resetFacets();
    }
    if(path.endsWith('organization')) {
        resetFacets();
    }
    if(path.endsWith('group')) {
        resetFacets();
    }


    $facets.each(function () {
        var key = $(this).data('name');
        var value = sessionStorage.getItem(buildKey(key));
        if(value != null) {
            $(this).find('.edp-basic-facet li').last().css('border-bottom', '1px dotted #dddddd');
            $(this).find('.edp-hidden-facet').show();
            $(this).find('.edp-less-facets').show();
            $(this).find('.edp-more-facets').hide();
        }
    });
});


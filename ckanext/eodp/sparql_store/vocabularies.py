# -*- coding: utf-8 -*-
"""
Providing graph objects and convenient functions for accessing several values used by DCAT-AP.
"""
import logging
from ckanext.eodp.config import CONFIG
from rdflib import Graph, URIRef

log = logging.getLogger(__name__)

MDRAUTHORITY = "http://publications.europa.eu/mdr/resource/authority"
ADMSVOC = "http://purl.org/adms"

VOC_FREQUENCIES = MDRAUTHORITY + "/frequency/skos/frequencies-skos.rdf"
VOC_FILETYPES = MDRAUTHORITY + "/file-type/skos/filetypes-skos.rdf"
VOC_LANGUAGES = MDRAUTHORITY + "/language/skos/languages-skos.rdf"
VOC_CORPORATEBODIES = MDRAUTHORITY + "/corporate-body/skos/corporatebodies-skos.rdf"
VOC_CONTINENTS = MDRAUTHORITY + "/continent/skos/continents-skos.rdf"
VOC_COUNTRIES = MDRAUTHORITY + "/country/skos/countries-skos.rdf"
VOC_PLACES = MDRAUTHORITY + "/place/skos/places-skos.rdf"
VOC_LICENCETYPES = ADMSVOC + "/licencetype/"
VOC_STATUS = ADMSVOC + "/status/"
VOC_PUBLISHERTYPES = ADMSVOC + "/publishertype/"

frequencies = Graph()
filetypes = Graph()
languages = Graph()
corporatebodies = Graph()
continents = Graph()
countries = Graph()
places = Graph()
licencetypes = Graph()
status = Graph()
publishertypes = Graph()

ENABLED = CONFIG.getboolean('sparql_store', 'vocabulary_resolution')


def _init_vocabularies():
    log.info('Loading SKOS Vocabulary')
    global ENABLED
    try:
        frequencies.parse(VOC_FREQUENCIES)
        # filetypes.parse(VOC_FILETYPES)
        languages.parse(VOC_LANGUAGES)
        corporatebodies.parse(VOC_CORPORATEBODIES)
        # continents.parse(VOC_CONTINENTS)
        # countries.parse(VOC_COUNTRIES)
        # places.parse(VOC_PLACES)
        # licencetypes.parse(VOC_LICENCETYPES, format="application/rdf+xml")
        # status.parse(VOC_STATUS)
        # publishertypes.parse(VOC_PUBLISHERTYPES)
        log.info('SKOS Vocabulary successfully loaded')
        log.info('SKOS Vocabulary Resolution enabled')
    except Exception as e:
        log.error('Error while loading SKOS Vocabulary ' + str(e))
        log.info('SKOS Vocabulary Resolution disabled')
        ENABLED = False


if ENABLED:
    _init_vocabularies()
else:
    log.info('SKOS Vocabulary Resolution disabled')


def is_available():
    return ENABLED


def _get_value(label):
    if label and label[0]:
        return label[0][1]
    else:
        return None


def frequency_label(ref, lang=None, default=None):
    label = frequencies.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def filetype_label(ref, lang=None, default=None):
    label = filetypes.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def language_label(ref, lang=None, default=None):
    label = languages.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def corporatebody_label(ref, lang=None, default=None):
    label = corporatebodies.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def continent_label(ref, lang=None, default=None):
    label = continents.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def country_label(ref, lang=None, default=None):
    label = countries.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def place_label(ref, lang=None, default=None):
    label = places.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def licencetype_label(ref, lang=None, default=None):
    label = licencetypes.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def status_label(ref, lang=None, default=None):
    label = status.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)


def publishertype_label(ref, lang=None, default=None):
    label = publishertypes.preferredLabel(URIRef(ref), lang, default)
    return _get_value(label)

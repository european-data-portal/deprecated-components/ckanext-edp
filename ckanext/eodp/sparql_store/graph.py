# -*- coding: utf-8 -*-
"""
Provide a graph object specific to the Pan European Open Data Portal.
It serves as container for all triples of the pan eodp. This graph is synchronized
with a quadruple store using SPARQL.
"""

from rdflib import Graph, Dataset, Literal
from rdflib.namespace import FOAF, DCTERMS, XSD, RDF
from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore

import ckanext.eodp.sparql_store.dcatap as dcatap
from ckanext.eodp.sparql_store.dcatap import CATALOG, CORPORATE_BODY, DCAT, V, ADMS, SPDX, LOCN

from SPARQLWrapper import DIGEST
from SPARQLWrapper.SPARQLExceptions import EndPointNotFound

from ckanext.eodp.config import CONFIG

import datetime
import urllib2

import logging
log = logging.getLogger(__name__)

SPARQL_ENDPOINT = CONFIG.getoptional('sparql_store', 'endpoint', "http://localhost:8890/sparql-auth")
SPARQL_STORE_GRAPH = CONFIG.getoptional('sparql_store', 'graph', "http://europeandataportal.eu/")

__author__ = 'sim'


class PanEODP(Dataset):
    """
    Specific graph handling pan european open data portal aspects.
    """

    def __init__(self):
        """
        Do connect to the sparql store as configured in the ckan ini file.
        Second, configure some readable namespace prefixes, and finally
        initialize a default dcat-ap catalog resource.
        """
        store = SPARQLUpdateStore()
        username = CONFIG.getoptional('sparql_store', 'username', "dba")
        password = CONFIG.getoptional('sparql_store', 'password', "dba")
        store.setCredentials(username, password)
        store.setHTTPAuth(DIGEST)
        super(PanEODP, self).__init__(store, SPARQL_STORE_GRAPH)

        self.open(SPARQL_ENDPOINT)
        self.connected = True

        self.bind('dcat', DCAT)
        self.bind('adms', ADMS)
        self.bind('v', V)
        self.bind('foaf', FOAF)
        self.bind('dct', DCTERMS)
        self.bind('spdx', SPDX)
        self.bind('locn', LOCN)

        self._init_common_catalog()

    def _init_common_catalog(self):
        """
        Do initialize the common catalog if not already exists
        """
        try:
            ng = self.get_context(CATALOG.Common)
            if len(ng) == 0:
                dcatap.catalog(ng, {'name': "Common", 'title': "Common Catalog",
                                                       'description': "Pan European Data catalog for any datasets "
                                                                      "not related to any other catalog."
                                                       })
                ng.set((CATALOG.Common, DCTERMS.publisher, CORPORATE_BODY.COM))

                self.touch_catalog(CATALOG.Common)
        except urllib2.URLError as e:
            log.warning("{}: {}".format(e.reason, SPARQL_ENDPOINT))
            log.warning("SPARQL store disabled")
            self.connected = False
        except EndPointNotFound:
            log.warning("SPARQL Endpoint not found, therefore the store is disabled")
            self.connected = False

    def touch_catalog(self, catalog):
        """
        Do setting the modified property of the given catalog to the current date and time
        :param catalog: the catalog to touch
        :type catalog: URIRef
        """
        ng = self.get_context(catalog)
        now = datetime.datetime.now()
        ng.set((catalog, DCTERMS.modified, Literal(now.isoformat(), datatype=XSD.dateTime)))


paneodp = PanEODP()

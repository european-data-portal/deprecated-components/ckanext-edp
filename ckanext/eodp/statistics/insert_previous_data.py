from ckanext.eodp.statistics import *
import openpyxl
import sys

from ckanext.eodp.model import DatasetsPerCategory, DatasetsPerCatalogue, DatasetsPerCountry, \
    DatasetsPerCountryCatalogue, DatasetsPerCountryCategory, DatasetsAssignedToDataCategoriesPerCountry, \
    DatasetsAssignedToDataCategoriesByCountryCatalogue

from ckanext.eodp.statistics import statistics_xls

import sqlalchemy.orm as orm
from ckan.model.meta import CkanCacheExtension, CkanSessionExtension
from ckan.model import extension
import ckan.lib.activity_streams_session_extension as activity

session = orm.scoped_session(orm.sessionmaker(
                autoflush=False,
                autocommit=False,
                expire_on_commit=False,
                extension=[CkanCacheExtension(),
                           CkanSessionExtension(),
                           extension.PluginSessionExtension(),
                           activity.DatasetActivitySessionExtension()],
            ))


def insert_previous_data():
    xls_file_url = sys.argv[3]
    xls_file = openpyxl.load_workbook(filename=xls_file_url)

    sheets = [xls_file.worksheets[0], xls_file.worksheets[1], xls_file.worksheets[2], xls_file.worksheets[3],
              xls_file.worksheets[4], xls_file.worksheets[5], xls_file.worksheets[6]]

    insert_09_2016(sheets)
    insert_12_2016(sheets)
    insert_03_2017(sheets)
    insert_06_2017(sheets)


def insert_09_2016(sheets):
    print "start inserting data from 09-2016"
    insert_category(sheets[0], '2016-09-30 00:00:00', 1)
    insert_catalogue(sheets[1], '2016-09-30 00:00:00', 1)
    insert_country_catalogue(sheets[2], '2016-09-30 00:00:00', 1)
    insert_country(sheets[3], '2016-09-30 00:00:00', 1)
    insert_country_category(sheets[4], '2016-09-30 00:00:00', 1)
    insert_assigned_1(sheets[5], '2016-09-30 00:00:00', 1)
    insert_assigned_2(sheets[6], '2016-09-30 00:00:00', 1)

    statistics_xls.add_latest_to_xls()


def insert_12_2016(sheets):
    print "start inserting data from 12-2016"
    insert_category(sheets[0], '2016-12-31 00:00:00', 2)
    insert_catalogue(sheets[1], '2016-12-31 00:00:00', 2)
    insert_country_catalogue(sheets[2], '2016-12-31 00:00:00', 2)
    insert_country(sheets[3], '2016-12-31 00:00:00', 2)
    insert_country_category(sheets[4], '2016-12-31 00:00:00', 2)
    insert_assigned_1(sheets[5], '2016-12-31 00:00:00', 4)
    insert_assigned_2(sheets[6], '2016-12-31 00:00:00', 4)

    statistics_xls.add_latest_to_xls()


def insert_03_2017(sheets):
    print "start inserting data from 03-2017"
    insert_category(sheets[0], '2017-03-31 00:00:00', 3)
    insert_catalogue(sheets[1], '2017-03-31 00:00:00', 3)
    insert_country_catalogue(sheets[2], '2017-03-31 00:00:00', 3)
    insert_country(sheets[3], '2017-03-31 00:00:00', 3)
    insert_country_category(sheets[4], '2017-03-31 00:00:00', 3)
    insert_assigned_1(sheets[5], '2017-03-31 00:00:00', 7)
    insert_assigned_2(sheets[6], '2017-03-31 00:00:00', 7)

    statistics_xls.add_latest_to_xls()


def insert_06_2017(sheets):
    print "start inserting data from 06-2017"
    insert_category(sheets[0], '2017-06-30 00:00:00', 3)
    insert_catalogue(sheets[1], '2017-06-30 00:00:00', 3)
    insert_country_catalogue(sheets[2], '2017-06-30 00:00:00', 3)
    insert_country(sheets[3], '2017-06-30 00:00:00', 3)
    insert_country_category(sheets[4], '2017-06-30 00:00:00', 3)
    insert_assigned_1(sheets[5], '2017-06-30 00:00:00', 7)
    insert_assigned_2(sheets[6], '2017-06-30 00:00:00', 7)

    statistics_xls.add_latest_to_xls()


def insert_category(sheet, date, col):
    print "inserting data per category for: " + date

    max_row = 15

    for row in range(2, max_row):
        datasets_per_category = DatasetsPerCategory()
        datasets_per_category.date = date
        datasets_per_category.label = sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_per_category.count = sheet[row][col].value
        else:
            datasets_per_category.count = 0
        datasets_per_category.Session = session
        datasets_per_category.save()

    print "finished inserting"


def insert_catalogue(sheet, date, col):
    print "inserting data per catalogue for: " + date

    max_row = 78

    for row in range(2, max_row):
        datasets_per_catalogue = DatasetsPerCatalogue()
        datasets_per_catalogue.date = date
        datasets_per_catalogue.label = sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_per_catalogue.count = sheet[row][col].value
        else:
            datasets_per_catalogue.count = 0
        datasets_per_catalogue.Session = session
        datasets_per_catalogue.save()

    print "finished inserting"


def insert_country_catalogue(sheet, date, col):
    print "inserting data per country and catalogue for: " + date

    max_row = 75

    for row in range(2, max_row):
        datasets_per_country_catalogue = DatasetsPerCountryCatalogue()
        datasets_per_country_catalogue.date = date
        datasets_per_country_catalogue.label = sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_per_country_catalogue.count = sheet[row][col].value
        else:
            datasets_per_country_catalogue.count = 0
        datasets_per_country_catalogue.Session = session
        datasets_per_country_catalogue.save()

    print "finished inserting"


def insert_country(sheet, date, col):
    print "inserting data per country for: " + date

    max_row = 36

    for row in range(2, max_row):
        datasets_per_country = DatasetsPerCountry()
        datasets_per_country.date = date
        datasets_per_country.label = sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_per_country.count = sheet[row][col].value
        else:
            datasets_per_country.count = 0
        datasets_per_country.Session = session
        datasets_per_country.save()

    print "finished inserting"


def insert_country_category(sheet, date, col):
    print "inserting data per country and category for: " + date

    spatials = ["AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "ES", "FI", "FR", "GB", "GR", "HR", "HU", "IE", "IT",
                "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO", "SE", "SI", "SK", "CH", "IS", "LI", "MD", "NO", "RS"
                ]

    max_row = (len(spatials) * 13) + (len(spatials) * 3)
    spatials_index = 0
    counter = 0
    row = 3

    while row < max_row:
        if counter == 13:
            counter = 0
            spatials_index += 1
            row += 3

        if counter == 0:
            print "inserting for spatial: " + spatials[spatials_index]

        datasets_per_country_category = DatasetsPerCountryCategory()
        datasets_per_country_category.date = date
        datasets_per_country_category.label = spatials[spatials_index] + ":" + sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_per_country_category.count = sheet[row][col].value
        else:
            datasets_per_country_category.count = 0
        datasets_per_country_category.Session = session
        datasets_per_country_category.save()

        counter += 1
        row += 1

    print "inserting data per country and category for: " + date


def insert_assigned_1(sheet, date, col):
    print "inserting data per assigned_to_data_categories_per_country for: " + date

    max_row = 37

    for row in range(3, max_row):
        datasets_assigned_to_data_categories_per_country = DatasetsAssignedToDataCategoriesPerCountry()
        datasets_assigned_to_data_categories_per_country.date = date
        datasets_assigned_to_data_categories_per_country.label = sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_assigned_to_data_categories_per_country.total_count = sheet[row][col].value
        else:
            datasets_assigned_to_data_categories_per_country.total_count = 0

        if sheet[row][col+1].value is not None:
            datasets_assigned_to_data_categories_per_country.assigned_count = sheet[row][col+1].value
        else:
            datasets_assigned_to_data_categories_per_country.assigned_count = 0
        datasets_assigned_to_data_categories_per_country.Session = session
        datasets_assigned_to_data_categories_per_country.save()


def insert_assigned_2(sheet, date, col):
    print "inserting data per assigned_to_data_categories_by_country_catalogue for: " + date

    max_row = 74

    for row in range(3, max_row):
        datasets_assigned_to_data_categories_per_country_catalogue = DatasetsAssignedToDataCategoriesByCountryCatalogue()
        datasets_assigned_to_data_categories_per_country_catalogue.date = date
        datasets_assigned_to_data_categories_per_country_catalogue.label = sheet[row][0].value.encode('utf-8', 'ignore').decode('utf-8')
        if sheet[row][col].value is not None:
            datasets_assigned_to_data_categories_per_country_catalogue.total_count = sheet[row][col].value
        else:
            datasets_assigned_to_data_categories_per_country_catalogue.total_count = 0

        if sheet[row][col+1].value is not None:
            datasets_assigned_to_data_categories_per_country_catalogue.assigned_count = sheet[row][col+1].value
        else:
            datasets_assigned_to_data_categories_per_country_catalogue.assigned_count = 0
        datasets_assigned_to_data_categories_per_country_catalogue.Session = session
        datasets_assigned_to_data_categories_per_country_catalogue.save()

from ckan.lib.cli import CkanCommand
from ckanext.eodp.statistics import statistics
from ckanext.eodp.statistics import statistics_xls
from ckanext.eodp.statistics import insert_previous_data

import sys


class Statistics(CkanCommand):
    """
    Different functions to post new data to the statistic tables:
    'datasets_per_category': Update the Datasets per Category Table,
    'datasets_per_catalogue' Update the Datasets per Catalogue Table,
    'datasets_per_country_catalogue': Update the Datasets per Country and Catalogue Table,
    'datasets_per_country': Update the Datasets per Country Table,
    'datasets_per_country_category': Update the Datasets per Country and Category Table,
    'datasets_assigned_to_data_categories_per_country': Update the Datasets assigned to Category per Country Table,
    'datasets_assigned_to_categories_by_country_and_catalogue': Update the Datasets assigned to Category per Country and Catalogue,
    'update_tables': update all tables by calling all functions for the statistic tables

    Functions to push new data to the xlsx file and upload it as a resource of the edp-dataset-statistics dataset:
    'add_latest_data_to_xlsx': Latest entries from the Statistic tables will be added to the xls file,
    'upload_xlsx': Upload the xlsx file to the resource of the statistics dataset,
    'update_xlsx': Call both functions 'add_latest_data_to_xls" and 'upload_xlsx'

    Function for calling all functions in a row:
    'update_tables_xlsx'
    """

    summary = __doc__.split('\n')[0]
    usage = __doc__
    max_args = None
    min_args = 1

    def __init__(self, name):
        super(Statistics, self).__init__(name)

    def command(self):
        cmd = self.args[0]

        self._load_config()

        if cmd == 'datasets_per_category':
            self.datasets_per_category()
        elif cmd == 'datasets_per_catalogue':
            self.datasets_per_catalogue()
        elif cmd == 'datasets_per_country_catalogue':
            self.datasets_per_country_catalogue()
        elif cmd == 'datasets_per_country':
            self.datasets_per_country()
        elif cmd == 'datasets_per_country_category':
            self.datasets_per_country_category()
        elif cmd == 'datasets_assigned_to_data_categories_per_country':
            self.datasets_assigned_to_data_categories_per_country()
        elif cmd == 'datasets_assigned_to_categories_by_country_and_catalogue':
            self.datasets_assigned_to_categories_by_country_and_catalogue()
        elif cmd == 'add_latest_data_to_xls':
            statistics_xls.add_latest_to_xls()
        elif cmd == 'upload_xls':
            statistics_xls.upload_xls()
        elif cmd == 'update_xlsx':
            statistics_xls.call_all_functions()
        elif cmd == 'change_encoding_xlsx':
            statistics_xls.change_encoding_xlsx()
        elif cmd == 'update_tables':
            self.call_all_functions()
        elif cmd == 'update_tables_xlsx':
            self.call_all_functions_with_xls()
        elif cmd == 'delete_tables':
            self.delete_tables()
        elif cmd == 'insert_previous_data':
            insert_previous_data.insert_previous_data()
        else:
            print 'Command %s not recognized' % cmd
            sys.exit(1)

    def datasets_per_category(self):
        statistics.datasets_per_category()

    def datasets_per_catalogue(self):
        statistics.datasets_per_catalogue()

    def datasets_per_country_catalogue(self):
        statistics.datasets_per_country_catalogue()

    def datasets_per_country(self):
        statistics.datasets_per_country()

    def datasets_per_country_category(self):
        statistics.datasets_per_country_category()

    def datasets_assigned_to_data_categories_per_country(self):
        statistics.datasets_assigned_to_data_categories_per_country()

    def datasets_assigned_to_categories_by_country_and_catalogue(self):
        statistics.datasets_assigned_to_categories_by_country_and_catalogue()

    def call_all_functions(self):
        statistics.call_all_functions()

    def call_all_functions_with_xls(self):
        statistics.call_all_functions_with_xls()

    def delete_tables(self):
        statistics.delete_tables()

    def post_previous_data(self):
        statistics.post_previous_data()

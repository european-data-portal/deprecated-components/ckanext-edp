# -*- coding: utf-8 -*-

import sys
import json

from ckan import model
from ckan.logic import get_action

import ckan.plugins.toolkit as tk

from ckan.lib.cli import CkanCommand

__author__ = 'sim'


class Translation(CkanCommand):
    """
    Hides and Seeks the translated and non-translated Datasets

    Usage:

    The commands should be run from the ckanext-eodp directory and expect
    a development.ini file to be present. Most of the time you will
    specify the config explicitly though::

        paster translation visible --config=../ckan/development.ini

    """

    summary = __doc__.split('\n')[0]
    usage = __doc__
    max_args = 2
    min_args = 0

    def __init__(self, name):
        super(Translation, self).__init__(name)

    def command(self):

        self._load_config()

        # We'll need a sysadmin user to perform most of the actions
        # We will use the sysadmin site user (named as the site_id)
        context = {'model': model, 'session': model.Session, 'ignore_auth': True}
        self.admin_user = get_action('get_site_user')(context, {})

        if len(self.args) == 0:
            self.parser.print_usage()
            sys.exit(1)

        cmd = self.args[0]
        if cmd == 'visible':
            self.visible(context)
        elif cmd == 'hide':
            self.hide(context)
        elif cmd == 'status':
            self.status(context)
        elif cmd == 'purge':
            self.purge(context)
        else:
            print "Command %s not recognized" % cmd

    def _load_config(self, load_site_user=True):
        super(Translation, self)._load_config(load_site_user)

    def visible(self, context):
        package_search = get_action('package_search')
        package_update = get_action('package_update')

        offset = 0
        count = 1
        while count > 0:
            search_result = package_search(context, {'q': '*:*', 'fq': '+state:draft', 'rows': 100, 'start': offset})
            if search_result['count']:
                results = search_result['results']
                count = len(results)
                print "activating %s datasets" % search_result['count']
                for package in results:
                    package['state'] = 'active'
                    package_update(context, package)
            else:
                count = 0

    def hide(self, context):
        package_search = get_action('package_search')
        package_update = get_action('package_update')

        offset = 0
        count = 1
        while count > 0:
            search_result = package_search(context, {'q': 'translated:false',
                                                     'fq': '+state:active',
                                                     'rows': 100,
                                                     'start': offset})
            if search_result['count']:
                results = search_result['results']
                count = len(results)
                print "hiding %s datasets" % search_result['count']
                for package in results:
                    package['state'] = 'draft'
                    package_update(context, package)
            else:
                count = 0

    def status(self, context):
        result = {}

        package_search = get_action('package_search')
        if len(self.args) > 1:
            q = 'organization:' + self.args[1]
        else:
            q = '*:*'

        search_result = package_search(context, {'q': q, 'fq': '+state:(active OR draft)', 'rows': 0, 'start': 0})
        if search_result['count']:
            result['count'] = search_result['count']

        search_result = package_search(context, {'q': q, 'rows': 0, 'start': 0})
        if search_result['count']:
            result['active'] = search_result['count']

        search_result = package_search(context, {'q': q + ' AND translated:false',
                                                 'fq': '+state:(active OR draft)',
                                                 'rows': 0, 'start': 0})
        if search_result['count']:
            result['not_translated'] = search_result['count']

        search_result = package_search(context, {'q': q, 'fq': '+state:draft', 'rows': 0, 'start': 0})
        if search_result['count']:
            result['draft'] = search_result['count']

        query = model.Session.query(model.Package.id).filter(model.Package.state == 'deleted')
        deleted = len(query.all())
        if deleted > 0:
            result['deleted'] = deleted

        print json.dumps(result, indent=3)

    def purge(self, context):
        if len(self.args) < 2:
            print "enter number to purge"
            return

        count = int(self.args[1])

        dataset_purge = get_action('dataset_purge')
        query = model.Session.query(model.Package.id).filter(model.Package.state == 'deleted')

        i = 0

        print "purged 0 datasets",
        sys.stdout.flush()

        for dataset in query:
            dataset_purge(context, {"id": dataset.id})
            i += 1
            print "\rpurged %s datasets" % i,
            if i >= count:
                break

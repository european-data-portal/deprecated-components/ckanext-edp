from ckan.lib.cli import CkanCommand
import sys
import requests
import json
import dateutil.parser
import datetime
import urllib
from ckan import model
from ckan.logic import get_action
from ckanext.eodp.trans import auto_translation
from ckanext.eodp.trans import *
import logging

log = logging.getLogger(__name__)

TRANS_SERVICE_URL = CONFIG.get(config_key, 'service')

class EdpUtil(CkanCommand):
    """
    Provides various utility commands for maintaining the EDP

    util update_datasets        - Perform a package_update on all datasets
    util reschedule_trans       - Reschedule all processing datasets

    """
    summary = __doc__.split('\n')[0]
    usage = __doc__
    max_args = None
    min_args = 1

    def __init__(self, name):
        super(EdpUtil, self).__init__(name)

    def command(self):

        cmd = self.args[0]
        self._load_config()

        if cmd == 'update_datasets':
            self.update_datasets()
        elif cmd == 'reschedule_trans':
            self.reschedule_trans()
        elif cmd == 'initdb':
            self.initdb()
        elif cmd == 'init_country_data':
            self.init_country_data()
        elif cmd == 'drop_country_data':
            self.drop_country_data()
        elif cmd == 'list_packages':
            self.list_packages()
        elif cmd == 'reschedule_trans2':
            self.check_translations()
        else:
            print 'Command %s not recognized' % cmd
            sys.exit(1)

    def reschedule_trans(self):
        processing_ids = TRANS_SERVICE_URL + 'processing-ids'
        try:
            processing = requests.get(processing_ids)
            processing = json.loads(processing.text)
        except:
            processing = []

        context = {'model': model, 'session': model.Session, 'ignore_auth': True}
        package_update = get_action('package_update')
        package_patch = get_action('package_patch')

        def reschedule(pkg):
            now = datetime.datetime.now().isoformat()
            if METAFIELD in pkg:
                metafield = pkg[METAFIELD]
                languages, original_language = auto_translation._get_target_languages_for_trans_service(pkg)
                if languages:
                    probe = metafield[languages[0]]
                    status = ''
                    if 'tag' in probe:
                        status = probe['tag']
                    if 'status' in metafield:
                        status = metafield['status']

                    if status in ['processing', 'updating', 'aborted']:
                        if languages:
                            issued = probe['issued']
                            issued = dateutil.parser.parse(issued)
                            due_date = datetime.datetime.now()-datetime.timedelta(days=3)
                            if issued < due_date:
                                for l in languages:
                                    metafield[l]['issued'] = now
                                try:
                                    package_patch(context, pkg)
                                    auto_translation.request_translation(context, pkg)
                                except Exception as e:
                                    print e.message
                                print 'Dataset %s rescheduled' % pkg['name']

        def callback(package):
            if package['id'] not in processing:
                reschedule(package)

        self._package_callback({
            'q': '-translated:[* TO *]',
            'sort': 'id asc',
            'include_drafts': True
        },
            callback
        )

    def _package_callback(self, params, callback):
        package_search = get_action('package_search')
        context = {'model': model, 'session': model.Session, 'ignore_auth': True}

        offset = 0
        count = 1
        i = 0

        basic_params = {
            'rows': 100,
            'start': offset
        }
        basic_params.update(params)

        init_search = package_search(context, basic_params)
        init_count = init_search['count']
        print "Found %d Dataset(s) to process." % init_count

        while count > 0:
            basic_params['start'] = offset
            result = package_search(context, basic_params)
            if result['count']:
                packages = result['results']
                count = len(packages)
                for package in packages:
                    i += 1
                    callback(package)
            else:
                count = 0
            offset += 100
        print "Processed %d Dataset(s)" % i


    def update_datasets(self):

        context = {'model': model, 'session': model.Session, 'ignore_auth': True}
        admin_user = get_action('get_site_user')(context, {})

        package_search = get_action('package_search')
        package_update = get_action('package_update')

        offset = 0
        count = 1
        i = 0
        while count > 0:
            try:
                search_result = package_search(context, {
                    'q': '-(source_identifier:*)',
                    'rows': 100,
                    'start': 0
                })
                if search_result['count']:
                    results = search_result['results']
                    count = len(results)
                    for package in results:
                        print "Processing " + package['name']
                        try:
                            if 'translation_meta' in package:
                                package['translation_meta']['auto_translation'] = False
                            else:
                                package['translation_meta'] = {}
                                package['translation_meta']['auto_translation'] = False
                            result = package_update(context, package)
                            i += 1
                        except Exception as e:
                            print e.message
                else:
                    count = 0
                offset += 100
            except Exception as e:
                print e.message

        print "Processed " + str(i) + " Datasets."

    def initdb(self):
        from ckanext.eodp.model import setup as db_setup
        db_setup()
        print 'DB tables created'

    def init_country_data(self):
        from ckanext.eodp.model import init_country_data
        init_country_data()
        print 'Country-Organization Mapping was created'

    def drop_country_data(self):
        from ckanext.eodp.model import drop_country_data
        drop_country_data()
        print 'Country-Organization Mapping was dropped'

    def list_packages(self, params, callback):
        api_key = API_KEY
        base_url = CALLBACK_URL
        # api_key = 'd4861ec7-b428-4f38-b35b-1fbcecbe009a'
         # base_url = 'http://europeandataportal.eu/data'

        headers = {
            'Content-type': 'application/json',
            'X-CKAN-API-Key': api_key
        }

        start = 0
        count = 1
        rows = 100

        while count > 0:
            try:
                url = base_url + "/api/3/action/package_search?rows=" + str(rows) + "&start=" + str(start)
                if params is not None:
                    for key in params.keys():
                        url += "&" + str(key) + "=" + str(params[key])
                response = requests.get(url, headers=headers)

                if response.status_code != 200:
                    status = "HTTP Status: " + str(response.status_code)
                    count = 0
                    print status
                else:
                    response_json = response.json()
                    if response_json['result']['count']:
                        results = response_json['result']['results']
                        count = len(results)
                        for package in results:
                            # print package['name']
                            callback(package)
                    else:
                        count = 0
                start += rows
            except Exception as e:
                print e.message

        return True

    def check_translations(self):

        url = CALLBACK_URL + '/translation/reschedule'

        params = {
            'q': 'state:draft',
            'sort': 'id asc',
            'include_drafts': True
        }

        headers = {
            'Content-type': 'application/json',
            'X-CKAN-API-Key': API_KEY,
        }

        def callback(package):
            if METAFIELD in package:
                metafield = package[METAFIELD]
                languages, original_language = auto_translation._get_target_languages_for_trans_service(package)
                if languages:
                    probe = metafield[languages[0]]
                    status = ''
                    if 'tag' in probe:
                        status = probe['tag']
                    if 'status' in metafield:
                        status = metafield['status']

                    if status in ['processing', 'updating', 'aborted']:
                        issued = probe['issued']
                        issued = dateutil.parser.parse(issued)
                        due_date = datetime.datetime.now() - datetime.timedelta(days=3)
                        # due_date = datetime.datetime.now()
                        if issued < due_date:
                            data = {
                                'name': package['name']
                            }
                            print package['name']
                            try:
                                response = requests.post(url, json=data, headers=headers)
                            except:
                                pass
                            if response.status_code != 200:
                                status = "HTTP Status: " + str(response.text)
                                print status

        self.list_packages(params, callback)

# this is a namespace package
try:
    import pkg_resources
    pkg_resources.declare_namespace(__name__)
except ImportError:
    import pkgutil
    __path__ = pkgutil.extend_path(__path__, __name__)

import ckan.plugins.toolkit as tk
import json

# categories = json.load(pkg_resources.resource_stream(__name__, 'etc/categories.json'))
# groups = tk.get_action('group_list')(data_dict={'groups': [category['name'] for category in categories]})
# categories_to_create = [category for category in categories if category['name'] not in groups]
#
# group_create = tk.get_action('group_create')
# for category in categories_to_create:
#     print category
#     # group_create(data_dict=category)
